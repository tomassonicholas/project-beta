import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something


def get_automobiles():
    get_response = requests.get(
        "http://project-beta-inventory-api-1:8000/api/automobiles"
    )
    content = json.loads(get_response.content)
    for vehicle in content["autos"]:
        data = {
            "vin": vehicle["vin"],
            "sold": vehicle["sold"],
        }
        try:
            requests.post("http://service-api:8000/api/vehicles/", json=data)
        except Exception as e:
            print(e, file=sys.stderr)


def poll(repeat=True):
    while True:
        print("Service poller polling for data")
        try:
            get_automobiles()
        except Exception as e:
            print(e, file=sys.stderr)

        if not repeat:
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
