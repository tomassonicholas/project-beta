from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from datetime import datetime
import json

from .models import (
    AutomobileVO,
    Technician,
    Appointment,
)
from common.json import ModelEncoder


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "vin",
        "customer",
        "vip",
        "id",
        "status",
    ]

    def get_extra_data(self, o):
        return {
            "technician": o.technician.__str__(),
        }


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


@require_http_methods(["GET", "POST"])
def api_list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            automobiles,
            encoder=AutomobileVOEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        automobile = AutomobileVO.objects.update_or_create(**content)
        return JsonResponse(
            automobile,
            encoder=AutomobileVOEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            content["date_time"] = datetime.strptime(
                content["date_time"],
                "%Y-%m-%d %H:%M",
            )
        except ValueError:
            content["date_time"] = datetime.fromisoformat(content["date_time"][:-1])

        content["technician"] = Technician.objects.get(
            employee_id=int(content["technician"])
        )
        # except:
        #     return JsonResponse({"message": "Invalid Status"})
        appointment = Appointment.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "canceled"
    appointment.save()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_finished_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "finished"
    appointment.save()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_details_technician(request, employee_id):
    if request.method == "GET":
        technician = Technician.objects.get(employee_id=employee_id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(employee_id=employee_id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_details_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            content["technician"] = Technician.objects.get(
                eployee_id=content["technician"]
            )
        except:
            return JsonResponse({"message": "Invalid Status"})
        appointment = Appointment.objects.udpate(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
