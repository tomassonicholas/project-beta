from django.urls import path
from .views import (
    api_list_appointments,
    api_list_automobiles,
    api_list_technicians,
    api_cancel_appointment,
    api_finished_appointment,
    api_details_technician,
    api_details_appointment,
)


urlpatterns = [
    path("vehicles/", api_list_automobiles, name="api_list_vehicles"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path(
        "appointments/<int:id>/cancel",
        api_cancel_appointment,
        name="api_appointment_cancel",
    ),
    path(
        "appointments/<int:id>/finish",
        api_finished_appointment,
        name="api_appointment_finish",
    ),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path(
        "technicians/<int:employee_id>/",
        api_details_technician,
        name="api_details_technician",
    ),
    path(
        "appointments/<int:id>/",
        api_details_appointment,
        name="api_details_appointment",
    ),
]
