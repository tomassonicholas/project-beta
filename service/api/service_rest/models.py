from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.PositiveIntegerField(unique=True, primary_key=True)

    def __str__(self) -> str:
        return f"{self.last_name}, {self.first_name}"

    def get_api_url(self):
        return reverse(
            "api_details_technician",
            kwargs={"employee_id": self.employee_id},
        )


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.PROTECT,
    )
    status = models.CharField(max_length=10, default="SUBMITTED")

    def __str__(self) -> str:
        return f"Appointment w/ {self.technician} on {self.date_time}"

    def get_api_url(self):
        return reverse("api_details_appointment", kwargs={"id": self.id})

    @classmethod
    def create(cls, **kwargs):
        try:
            AutomobileVO.objects.filter(vin=kwargs["vin"])
            kwargs["vip"] = True
        except AutomobileVO.DoesNotExist:
            kwargs["vip"] = False

        new_appointment = cls(**kwargs)
        new_appointment.save()
        return new_appointment
