# CarCar

Team:

- Nick Tomasso - Services
- Wagner Jacinto - Sales

## Design

## Service microservice

I have 3 different models Technician, AutomobileVO and Appointment as outlined in the project requirements. I decided to handle the VIP status tag on the backend as I felt a custom class method for creation of Appointments would be the simplest way to handle the data comparison.

The front end has the bulk of the customizations, the largest change from the project plan was the descision to use an offcanvas navbar rather than trying to pile all of the links in the header. I felt it looked cleaner and more professional this way given the amount of links on the website. The Offcanvas has buttons for each of the categories on the site and these buttons contain dropdowns to the specific site locations you may wish to reach.

I also discovered a better way to make table near the end of the project, which can be seen in the code for Appointment List and Appointment history. Instead of writing 7 or eight lines of table data in my map function I call another map on an obj containing the keys and labels for the table colums and use that to exract the data and generate the table.

The Inventory pages I did my best to make them look exactly like what was shown in the project desgin screenshots.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.
