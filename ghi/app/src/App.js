import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturesList from './components/inventory/ManufacturesList';
import ManufacturesCreate from './components/inventory/ManufacturesCreate';
import ModelsList from './components/inventory/ModelsList';
import ModelsCreate from './components/inventory/ModelsCreate';
import AutomobileList from './components/inventory/AutomobileList';
import AutomobilesCreate from './components/inventory/AutomobileCreate';
import TechniciansList from './components/service/TechniciansList';
import TechnicianCreate from './components/service/TechniciansCreate';
import AppointmentList from './components/service/AppointmentsList';
import AppointmentCreate from './components/service/AppointmentsCreate';
import AppointmentHistory from './components/service/AppointmentHistory';
import ListSale from './components/sales/ListSale';
import ListCustomer from './components/sales/ListCustomer';
import ListSalesPeople from './components/sales/ListSalesperson';
import CreateCustomer from './components/sales/CreateCustomer';
import CreateSalesperson from './components/sales/CreateSalesperson';
import CreateSale from './components/sales/CreateSale';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='manufacturers/' element={<ManufacturesList />} />
          <Route path='manufacturers/new' element={<ManufacturesCreate />} />
          <Route path='models/' element={<ModelsList />} />
          <Route path='models/new' element={<ModelsCreate />} />
          <Route path='automobiles/' element={<AutomobileList />} />
          <Route path='automobiles/new' element={<AutomobilesCreate />} />
          <Route path='technicians' element={<TechniciansList />} />
          <Route path='technicians/new' element={<TechnicianCreate />} />
          <Route path='appointments' element={<AppointmentList />} />
          <Route path='appointments/new' element={<AppointmentCreate />} />
          <Route path='appointments/history' element={<AppointmentHistory />} />
          <Route path='sales' element={<ListSale />} />
          <Route path='customers' element={<ListCustomer />} />
          <Route path='employees' element={<ListSalesPeople />} />
          <Route path='sales/new' element={<CreateSale />} />
          <Route path='customers/new' element={<CreateCustomer />} />
          <Route path='employees/new' element={<CreateSalesperson />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
