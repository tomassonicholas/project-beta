import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="btn btn-black" type="button" data-bs-toggle="offcanvas" data-bs-target="#demo">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="offcanvas offcanvas-end" id="demo" data-bs-theme="dark">
          <div className="offcanvas-header">
            <h1 className="offcanvas-title">CarCar</h1>
            <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas"></button>
          </div>
          <div className="offcanvas-body">
            <ul className='navbar-nav justify-content-end flex-grow-1 pe-3'>
              <li className="nav-item dropdown mb-3">
                <button type="button" className="btn btn-success dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                  Manufactureres
                </button>
                <ul className="dropdown-menu">
                  <li>
                    <NavLink className="dropdown-item" to='manufacturers'>View Manufacturers</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='manufacturers/new'>Add A Manufacturer</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown mb-3">
                <button type="button" className="btn btn-success dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                  Models
                </button>
                <ul className="dropdown-menu">
                  <li>
                    <NavLink className="dropdown-item" to='models'>View Models</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='models/new'>Add A Model</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown mb-3">
                <button type="button" className="btn btn-success dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                  Automobiles
                </button>
                <ul className="dropdown-menu">
                  <li>
                    <NavLink className="dropdown-item" to='automobiles'>View Automobiles</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='automobiles/new'>Add An Automobile</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown mb-3">
                <button type="button" className="btn btn-success dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales
                </button>
                <ul className="dropdown-menu">
                  <li>
                    <NavLink className="dropdown-item" to='customers/new'>Create Customer</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='sales/new'>Create Sale</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='employees/new'>Create Salesperson</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='customers'>List Customers</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='sales'>List Sales</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='employees'>List Salespeople</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown mb-3">
                <button type="button" className="btn btn-success dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                  Service
                </button>
                <ul className="dropdown-menu">
                  <li>
                    <NavLink className="dropdown-item" to='technicians'>View Technicians</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='technicians/new'>Add A New Technician</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='appointments'>View Appointments</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='appointments/new'>Create An Appointment</NavLink>
                  </li>
                  <li><hr className="dropdown-divider" /></li>
                  <li>
                    <NavLink className="dropdown-item" to='appointments/history'>Appointment History</NavLink>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
