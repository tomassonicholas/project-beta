import { useEffect, useState } from "react"
import { Link } from "react-router-dom"


function AppointmentList () {
    const [listData, setListData] = useState([])

    const columns =[
        { label: "Vin", accessor: "vin" },
        { label: "VIP", accessor: "vip" },
        { label: "Customer", accessor: "customer" },
        { label: "Date", accessor: "date" },
        { label: "Time", accessor: "time" },
        { label: "Technician", accessor: "technician" },
        { label: "Reason", accessor: "reason" },
        { label: "Status", accessor: "status" },
        { label: "Actions", accessor: 'actions'},
    ]

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/'

        const response = await fetch(url)

        const data = await response.json()
        const deletions = []

        for (const [i, appointment] of data.appointments.entries()){
            const date = new Date(appointment.date_time)
            appointment['date'] = date.toLocaleDateString()
            appointment['time'] = date.toLocaleTimeString()
            if (appointment.status !== 'SUBMITTED'){
                deletions.push(i)
            }
        }
        deletions.forEach( x => {
            delete data.appointments[x]
        })

        setListData(data.appointments)
    }

    useEffect(() => {
        fetchData()
    }, [])


    const handleSubmit = async (e) => {
        const url = `http://localhost:8080${e.target.value}`

        const fetchConfig = {
            method: 'put',
            headers: {
                'Content-Type': 'application.json',
            },
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            fetchData()
        }
    }

    return (
        <div className="container mt-3">
            <div className="shadow card">
                <h1 className="justify-content-center d-flex mb-3">Appointment History</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                        {columns.map(({ label, accessor }) => {
                            return <th key={accessor} scope="col">{label}</th>
                        })}
                        </tr>
                    </thead>
                    <tbody>
                        {listData.map(x => {
                            return (
                                <tr key={x.id}>
                                    {columns.map(({accessor}) => {
                                        if (accessor !== 'actions'){
                                            let tData = x[accessor] ? x[accessor] : "---"
                                            if (accessor === 'vip'){
                                                tData = x[accessor] === true ? 'Yes' : 'No'
                                            }
                                            return <td key={accessor}>{tData}</td>
                                        }
                                        return (
                                            <td key={`${x.vin} actions`}>
                                                <button onClick={handleSubmit} className="btn btn-danger" value={`${x.href}cancel`}>Cancel</button>
                                                <button onClick={handleSubmit} className="btn btn-success" value={`${x.href}finish`}>Finish</button>
                                            </td>
                                        )
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default AppointmentList
