import { useState, useEffect } from "react"
import { Link } from "react-router-dom"


function TechniciansList () {
    const [listData, setListData] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'

        const response = await fetch(url)

        const data = await response.json()

        setListData(data.technicians)
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleDelete = async (e) => {
        const url = `http://localhost:8080${e.target.value}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application.json',
            },
        }

            const response = await fetch(url, fetchConfig)

        if (response.ok) {
            fetchData()
        }else{

        }

    }

    return (
        <div className='container mt-3'>
            <div className='shadow card'>
                <h1 className="d-flex justify-content-center">Technicians</h1>
                <table className='table table-striped'>
                    <thead>
                    <tr>
                        <th key={'Name'} scope='col'>First Name</th>
                        <th key={'Name'} scope='col'>Last Name</th>
                        <th key={'Name'} scope='col'>Employee ID</th>
                        <th key={'Actions'} scope='col'>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        {listData.map(x => {
                            return (
                                <tr key={`row for ${x.employee_id}`}>
                                    <td key={`${x.first_name}`}>{x.first_name}</td>
                                    <td key={`${x.last_name}`}>{x.last_name}</td>
                                    <td key={`${x.employee_id}`}>{x.employee_id}</td>
                                    <td key={`${x.vin} actions`}>
                                        <button onClick={handleDelete} className="btn btn-danger" value={x.href}>Delete</button>
                                    </td>
                                </tr>

                            )
                        })}
                    </tbody>
                </table>
            </div>
            <div className='container'>
                <button className="btn btn-primary">
                    <Link to={'new'} className="btn-primary">New</Link>
                </button>
            </div>
        </div>
    )
}

export default TechniciansList
