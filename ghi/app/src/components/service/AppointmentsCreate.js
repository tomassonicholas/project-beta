import { useEffect, useState } from "react"


function AppointmentCreate () {
    const [formData, setFormData] = useState({
        date: '',
        time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: '',
    })
    const [technicians, setTechnicians] = useState([])

    const fetchData = async () => {
        const techUrl = 'http://localhost:8080/api/technicians/'

        const techResponse = await fetch(techUrl)

        const techData = await techResponse.json()

        setTechnicians(techData.technicians)
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleFormChange = (e) => {
        const data = e.target.value
        const name = e.target.name
        setFormData({...formData, [name]: data})
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault()

        const url = 'http://localhost:8080/api/appointments/'

        const data = {
            'date_time': formData.date.concat(' ', formData.time),
            'reason': formData.reason,
            'vin': formData.vin,
            'customer': formData.customer,
            'technician': formData.technician,
        }

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        try {
            const response = await fetch(url, fetchConfig)

            if (response.ok) {
                setFormData({
                    date: '',
                    time:'',
                    reason: '',
                    vin: '',
                    customer: '',
                    technician: '',
                })
            }
        } catch (e) {

        }

    }

    return (
        <div className='container d-flex'>
            <div className='shadow card p-4'>
                <h1 className='d-flex justify-content-center mb-3'>Add an Appointment</h1>
                <div className='container'>
                    <form className='form-floating row g-3 mb-3' onSubmit={handleFormSubmit}>
                        <div className='form-floating mb-3 input-group'>
                            <span className="input-group-text">Date / Time</span>
                            <input onChange={handleFormChange} value={formData.date} required className='form-control' id='date' name='date' type='date' placeholder='SubiSue' />
                            <input onChange={handleFormChange} value={formData.time} required className='form-control' id='time' name='time' type='time' placeholder='SubiSue' />
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.reason} required className='form-control' id='reason' name='reason' placeholder='SubiSue' />
                            <label htmlFor='inputReason'>Reason</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.vin} required className='form-control' id='vin' name='vin' type='text' placeholder='SubiSue' />
                            <label htmlFor='inputVin'>Vin</label>
                        </div>
                        <div className='form-floating mb-3 input-group'>
                            <span className="input-group-text">Customer First and Last Name (ie: John Smith)</span>
                            <input onChange={handleFormChange} value={formData.customer} required className='form-control' id='customer' name='customer' type='text' placeholder='0' />
                        </div>
                        <div className='form-floating mb-3'>
                            <select onChange={handleFormChange} value={formData.technician} required className='form-control' id='technician' name='technician' type='text' placeholder='0' >
                                <option>Technicians</option>
                                {technicians.map(x => {
                                    return (
                                        <option value={x.employee_id} key={x.employee_id}>
                                            {x.first_name} {x.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div>
                            <button className='btn btn-primary'>Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AppointmentCreate
