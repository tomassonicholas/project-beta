import { useState, useEffect } from "react";


function AppointmentHistory() {
    const [listData, setListData] = useState([])

    const columns =[
        { label: "Vin", accessor: "vin" },
        { label: "VIP", accessor: "vip" },
        { label: "Customer", accessor: "customer" },
        { label: "Date", accessor: "date" },
        { label: "Time", accessor: "time" },
        { label: "Technician", accessor: "technician" },
        { label: "Reason", accessor: "reason" },
        { label: "Status", accessor: "status" },
    ]

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/'

        const response = await fetch(url)

        const data = await response.json()

        data.appointments.forEach(x => {
            let date = new Date(x.date_time)
            x['date'] = date.toLocaleDateString()
            x['time'] = date.toLocaleTimeString()
        })

        setListData(data.appointments)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="container mt-3">
            <div className="shadow card">
                <h1 className="justify-content-center d-flex mb-3">Appointment History</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                        {columns.map(({ label, accessor }) => {
                            return <th key={accessor} scope="col">{label}</th>
                        })}
                        </tr>
                    </thead>
                    <tbody>
                        {listData.map(x => {
                            return (
                                <tr key={x.id}>
                                    {columns.map(({accessor}) => {
                                        let tData = x[accessor] ? x[accessor] : "---"
                                        if (accessor === 'vip'){
                                            tData = x[accessor] === true ? 'Yes' : 'No'
                                        }
                                        return <td key={accessor}>{tData}</td>
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default AppointmentHistory
