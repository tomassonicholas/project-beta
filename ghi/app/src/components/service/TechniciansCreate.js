import { useState, useEffect } from "react"


function TechnicianCreate () {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name:'',
        employee_id: '',
    })


    const handleFormChange = (e) => {
        const data = e.target.value
        const name = e.target.name
        setFormData({...formData, [name]: data})
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault()

        const url = 'http://localhost:8080/api/technicians/'

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        try {
            const response = await fetch(url, fetchConfig)

            if (response.ok) {
                setFormData({
                    first_name: '',
                    last_name:'',
                    employee_id: '',
                })
            }
        } catch (e) {

        }

    }

    return (
        <div className='container'>
            <div className='shadow card p-4'>
                <h1 className='d-flex justify-content-center mb-3'>Add an Technician</h1>
                <div className='container'>
                    <form className='form-floating row g-3 mb-3' onSubmit={handleFormSubmit}>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.first_name} required className='form-control' id='first_name' name='first_name' type='text' placeholder='SubiSue' />
                            <label htmlFor='inputFirstName'>First Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.last_name} required className='form-control' id='last_name' name='last_name' type='text' placeholder='SubiSue' />
                            <label htmlFor='inputLastName'>Last Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.employee_id} required className='form-control' id='employee_id' name='employee_id' type='number' placeholder='SubiSue' />
                            <label htmlFor='inputEmployeeID'>Employee ID</label>
                        </div>
                        <div>
                            <button className='btn btn-primary'>Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default TechnicianCreate
