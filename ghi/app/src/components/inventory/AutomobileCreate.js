import React, { useEffect, useState } from 'react'

function AutomobilesCreate() {
    const [formData, setFormData] = useState({
        color: '',
        year:'',
        vin: '',
        model_id: '',
    })
    const [models, setModels] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'

        const response = await fetch(url)

        const data = await response.json()

        setModels(data.models)
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleFormChange = (e) => {
        const data = e.target.value
        const name = e.target.name
        setFormData({...formData, [name]: data})
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault()

        const url = 'http://localhost:8100/api/automobiles/'

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        try {
            const response = await fetch(url, fetchConfig)

            if (response.ok) {
                setFormData({
                    color: '',
                    year:'',
                    vin: '',
                    model_id: '',
                })
            }
        } catch (e) {

        }

    }

    return (
        <div className='container'>
            <div className='shadow card p-4'>
                <h1 className='d-flex justify-content-center mb-3'>Add an Automobile</h1>
                <div className='container'>
                    <form className='form-floating row g-3 mb-3' onSubmit={handleFormSubmit}>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.color} required className='form-control' id='color' name='color' type='text' placeholder='SubiSue' />
                            <label htmlFor='inputName'>Color</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.year} required className='form-control' id='year' name='year' type='text' placeholder='SubiSue' />
                            <label htmlFor='inputName'>Year</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.vin} required className='form-control' id='vin' name='vin' type='text' placeholder='SubiSue' />
                            <label htmlFor='inputName'>Vin</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <select onChange={handleFormChange} value={formData.model_id} required className='form-control' id='model_id' name='model_id' type='text' placeholder='0' >
                                <option>Model</option>
                                {models.map(x => {
                                    return (
                                        <option value={x.id} key={x.id}>
                                            {x.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div>
                            <button className='btn btn-primary'>Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AutomobilesCreate
