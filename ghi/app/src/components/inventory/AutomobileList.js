import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

function AutomobileList() {
    const [listData, setListData] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/'

        const response = await fetch(url)

        const data = await response.json()

        setListData(data.autos)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className='container mt-3'>
            <div className='shadow card mb-3'>
            <h1 className='d-flex justify-content-center mb-3'>Automobiles</h1>
                <table className='table table-striped'>
                    <thead>
                    <tr>
                        <th key={'Name'} scope='col'>Vin</th>
                        <th key={'Color'} scope='col'>Color</th>
                        <th key={'Year'} scope='col'>Year</th>
                        <th key={'Model'} scope='col'>Model</th>
                        <th key={'Manufacturer'} scope='col'>Manufacturer</th>
                        <th key={'Sold'} scope='col'>Sold</th>
                    </tr>
                    </thead>
                    <tbody>
                        {listData.map(x => {
                            if (x.sold === true) {
                                x.sold = 'Yes'
                            } else {
                                x.sold = 'No'
                            }
                            return (
                                <tr key={`row for ${x.name}`}>
                                    <td key={`${x.vin}`}>{x.vin}</td>
                                    <td key={`${x.color}`}>{x.color}</td>
                                    <td key={`${x.year}`}>{x.year}</td>
                                    <td key={`${x.model.name}`}>{x.model.name}</td>
                                    <td key={`${x.model.manufacturer.name}`}>{x.model.manufacturer.name}</td>
                                    <td key={`${x.sold}`}>{x.sold}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
            <div className='container'>
                <button className="btn btn-primary">
                    <Link to={'new'} className="btn-primary">New</Link>
                </button>
            </div>
        </div>
    )
}

export default AutomobileList
