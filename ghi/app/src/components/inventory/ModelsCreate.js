import React, { useEffect, useState } from 'react'

function ModelsCreate() {
    const [formData, setFormData] = useState({
        name: '',
        picture_url:'',
        manufacturer_id: '',
    })
    const [manufacts, setManufacts] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'

        const response = await fetch(url)

        const data = await response.json()

        setManufacts(data.manufacturers)
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleFormChange = (e) => {
        const data = e.target.value
        const name = e.target.name
        setFormData({...formData, [name]: data})
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault()

        const url = 'http://localhost:8100/api/models/'

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        try {
            const response = await fetch(url, fetchConfig)

            if (response.ok) {
                setFormData({
                    name: '',
                    picture_url:'',
                    manufacturer_id: '',
                })
            }
        } catch (e) {

        }

    }

    return (
        <div className='container mt-3'>
            <div className='shadow card p-4'>
                <h1 className='d-flex justify-content-center mb-3'>Add a Model</h1>
                <div className='container'>
                    <form className='form-floating row g-3 mb-3' onSubmit={handleFormSubmit}>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.name} required className='form-control' id='name' name='name' type='text' placeholder='SubiSue' />
                            <label htmlFor='inputName'>Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.picture_url} required className='form-control' id='picture_url' name='picture_url' type='text' placeholder='SubiSue' />
                            <label htmlFor='inputName'>Picture Url</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <select onChange={handleFormChange} value={formData.manufacturer_id} required className='form-control' id='manufacturer_id' name='manufacturer_id' type='text' placeholder='0' >
                                <option>Manufacturer</option>
                                {manufacts.map(x => {
                                    return (
                                        <option value={x.id} key={x.id}>
                                            {x.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div>
                            <button className='btn btn-primary'>Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ModelsCreate
