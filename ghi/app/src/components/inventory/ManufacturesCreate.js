import React, { useState } from 'react'

function ManufacturesCreate() {
    const [formData, setFormData] = useState({
        name: '',
    })

    const handleFormChange = (e) => {
        const data = e.target.value
        const name = e.target.name
        setFormData({...formData, [name]: data})
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault()

        const url = 'http://localhost:8100/api/manufacturers/'

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        try {
            const response = await fetch(url, fetchConfig)

            if (response.ok) {
                setFormData({
                    name: '',
                })
            }
        } catch (e) {

        }

    }

    return (
        <div className='container mt-3'>
            <div className='shadow card p-4'>
                <h1 className='d-flex justify-content-center mb-3'>Add a Manufacturer</h1>
                <div className='container'>
                    <form className='form-floating row g-3 mb-3' onSubmit={handleFormSubmit}>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.name} required className='form-control' id='name' name='name' type='text' placeholder='Name' />
                            <label htmlFor='inputName'>Name</label>
                        </div>
                        <div className='mb-3'>
                            <button className='btn btn-primary'>Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ManufacturesCreate
