import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

function ManufacturesList() {
    const [listData, setListData] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'

        const response = await fetch(url)

        const data = await response.json()

        setListData(data.manufacturers)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className='container mt-3'>
            <div className='shadow card mb-3'>
                <h1 className='d-flex justify-content-center mb-3'>Manufacturers</h1>
                <table className='table table-striped'>
                    <thead>
                    <tr>
                        <th key={'Name'} scope='col'>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                        {listData.map(x => {
                            return (
                                <tr key={`row for ${x.name}`}>
                                    <td key={`${x.name}`}>{x.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
            <div className='container'>
                <button className="btn btn-primary">
                    <Link to={'new'} className="btn-primary">New</Link>
                </button>
            </div>
        </div>
    )
}

export default ManufacturesList
