import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

function ModelsList() {
    const [listData, setListData] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'

        const response = await fetch(url)

        const data = await response.json()

        setListData(data.models)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className='container mt-3'>
            <div className='shadow card mb-3'>
            <h1 className='d-flex justify-content-center mb-3'>Models</h1>
                <table className='table table-striped'>
                    <thead>
                    <tr>
                        <th key={'Name'} scope='col'>Name</th>
                        <th key={'Name'} scope='col'>Manufacturer</th>
                        <th key={'Name'} scope='col'>Picture</th>
                    </tr>
                    </thead>
                    <tbody>
                        {listData.map(x => {
                            return (
                                <tr key={`row for ${x.name}`}>
                                    <td key={`${x.name}`}>{x.name}</td>
                                    <td key={`${x.manufacturer.name} ${x.id}`}>{x.manufacturer.name}</td>
                                    <td key={`${x.picture_url}`}><img className='img-thumbnail' src={x.picture_url} alt={`${x.picture_url}`}/></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
            <div className='container'>
                <button className="btn btn-primary">
                    <Link to={'new'} className="btn-primary">New</Link>
                </button>
            </div>
        </div>
    )
}

export default ModelsList
