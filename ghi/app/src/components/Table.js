export default function Table({columns, data}){

    return(
        <table className="table table-striped">
                    <thead>
                        <tr>
                        {columns.map(({ label, accessor }) => {
                            return <th key={accessor} scope="col">{label}</th>
                        })}
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(x => {
                            return (
                                <tr key={x.id}>
                                    {columns.map(({accessor}) => {
                                        let tData = x[accessor] ? x[accessor] : "---"
                                        return <td key={accessor}>{tData}</td>
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
    )
}
