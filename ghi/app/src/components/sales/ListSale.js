import Table from "../Table";
import { useState, useEffect } from "react";

export default function ListSale(){
    const [list, updateList] = useState([[]])
    const fetchURL = "http://localhost:8090/api/sales/"
    const columns = [
        {label:"Vin", accessor:"automobile"},
        {label:"Salesperson", accessor:"salesperson"},
        {label:"Customer", accessor:"customer"},
        {label:"Sale Price", accessor:"price"},
    ]

    const  fetchData = async () => {
        const response = await fetch(fetchURL)
        if (response.ok){
            const data = await response.json()
            updateList(data)
        }
    }

    useEffect(() =>{
        fetchData()
    },[])

    return (
        <div className='container mt-3'>
            <div className='shadow card'>
            <h1>List of Sales</h1>
            <Table columns={columns} data={list} />
            </div>
        </div>
    )
}
