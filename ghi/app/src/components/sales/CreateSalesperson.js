import CreateForm from "../CreateForm"
import { useState } from "react"

export default function CreateSalesperson(){
    const fields = [
        {label:"First Name", accessor:"first_name"},
        {label:"Last Name", accessor:"last_name",},
        {label:"Employee ID", accessor:"employee_id",},
    ]
    const url = "http://localhost:8090/api/salespeople/"
    const [formData, setFormData] = useState({})

    const handleFormChange = (e) => {
        const data = e.target.value
        const name = e.target.name
        setFormData({...formData, [name]: data})
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault()

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        try {
            const response = await fetch(url, fetchConfig)

            if (response.ok) {
                setFormData({
                    first_name: '',
                    last_name:'',
                    employee_id: "",
                })
            }
        } catch (e) {

        }

    }
    return (
        <div className='container'>
            <div className='shadow card p-4'>
                <h1 className='d-flex justify-content-center mb-3'>Add an Salesperson</h1>
                <div className='container'>
                <CreateForm fields={fields} changeFunc={handleFormChange} submitFunc={handleFormSubmit} data={formData}/>
                </div>
            </div>
        </div>
    )
}
