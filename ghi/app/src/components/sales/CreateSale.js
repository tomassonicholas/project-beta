import { useEffect, useState } from "react"

export default function CreateSale(){
    const url = "http://localhost:8090/api/sales/"
    const [formData, setFormData] = useState({})
    const [selectors, setSelectors] = useState([[],[],[]])
    const fetchData = async() => {
        const tempSelectors= []
        const baseUrl = "http://localhost:8090/api/"
        const destinations = ["automobiles/", "salespeople/", 'customers/']
        for (let dest of destinations){
            try{
                let response = await fetch(baseUrl+dest)
                if (response.ok){
                    let data = await response.json()
                    tempSelectors.push(data)
                }
            }catch (e){
                console.error("Could not complete fetch")
            }
        }
        setSelectors(tempSelectors)
        console.log(selectors)
    }
    useEffect(() => {
        fetchData()
    },[])

    const handleFormChange = (e) => {
        const data = e.target.value
        const name = e.target.name
        setFormData({...formData, [name]: data})
    }

    const handleFormSubmit = async (e) => {
        e.preventDefault()

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        try {
            const response = await fetch(url, fetchConfig)

            if (response.ok) {
                setFormData({
                    automobile: '',
                    salesperson:'',
                    customer: '',
                    price:"",
                })
            }
        } catch (e) {

        }

    }

    return (
        <div className='container'>
            <div className='shadow card p-4'>
                <h1 className='d-flex justify-content-center mb-3'>Add a Sale</h1>
                <div className='container'>
                <form className='form-floating row g-3 mb-3' onSubmit={handleFormSubmit}>
                    <div className='form-floating mb-3'>
                        <select onChange={handleFormChange} value={formData.automobile} required className='form-control' id='automobile' name='automobile' type='text' placeholder='0' >
                            <option>Automobile</option>
                            {selectors[0].map(x => {
                                return (
                                    <option value={x.vin} key={x.vin}>
                                        {x.vin}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className='form-floating mb-3'>
                        <select onChange={handleFormChange} value={formData.salesperson} required className='form-control' id='salesperson' name='salesperson' type='text' placeholder='0' >
                            <option>Salesperson</option>
                            {selectors[1].map(x => {
                                return (
                                    <option value={x.employee_id} key={x.employee_id}>
                                        {x.first_name} {x.last_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className='form-floating mb-3'>
                        <select onChange={handleFormChange} value={formData.customer} required className='form-control' id='customer' name='customer' type='text' placeholder='0' >
                            <option>Customer</option>
                            {selectors[2].map(x => {
                                return (
                                    <option value={x.phone_number} key={x.phone_number}>
                                        {x.first_name} {x.last_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className='form-floating mb-3'>
                        <input onChange={handleFormChange} value={formData.price} required className='form-control' id="price" name="price" type='text' placeholder='SubiSue' />
                        <label htmlFor='inputPrice'>Price</label>
                    </div>
                    <div>
                        <button className='btn btn-primary'>Create</button>
                    </div>
            </form>
                </div>
            </div>
        </div>
    )
}
