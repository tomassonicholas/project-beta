import Table from "../Table";
import { useState, useEffect } from "react";

export default function ListSalesPeople(){
    const [list, updateList] = useState([[]])
    const fetchURL = "http://localhost:8090/api/salespeople/"
    const columns = [
        {label:"First Name", accessor:"first_name"},
        {label:"Last Name", accessor:"last_name"},
        {label:"Employee ID", accessor:"employee_id"},
    ]

    const  fetchData = async () => {
        const response = await fetch(fetchURL)
        if (response.ok){
            const data = await response.json()
            updateList(data)
        }
    }

    useEffect(() =>{
        fetchData()
    },[])

    return (
        <div className='container mt-3'>
            <div className='shadow card'>
            <h1>List of Salespeople</h1>
            <Table columns={columns} data={list} />
            </div>
        </div>
    )
}
