export default function CreateForm ({fields, changeFunc, submitFunc, data, selectors}){

    return (
        <form className='form-floating row g-3 mb-3' onSubmit={submitFunc}>
            {fields.map(x =>{
                if (x.type != "selector"){
                    return (
                    <div className='form-floating mb-3'>
                        <input onChange={changeFunc} value={data[x.accessor]} required className='form-control' id={x.accessor} name={x.accessor} type='text' placeholder='SubiSue' />
                        <label htmlFor='inputFirstName'>{x.label}</label>
                    </div>
                )
                }else{
                    <div className='form-floating mb-3'>
                            <select onChange={changeFunc} value={data[x.accessor]} required className='form-control' id='technician' name='technician' type='text' placeholder='0' >
                                <option>{x.label}</option>
                                {selectors.map(x => {
                                    return (
                                        <option value={x.employee_id} key={x.employee_id}>
                                            {x.first_name} {x.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                }

            })}
            <div>
                <button className='btn btn-primary'>Create</button>
            </div>
        </form>
    )
}
