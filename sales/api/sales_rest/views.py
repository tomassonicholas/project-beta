from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import (
    Salesperson,
    Sale,
    Customer,
    AutomobileVO,
)
import json


# Create your views here.
class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id"]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "phone_number"]


class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["id", "price"]

    def get_extra_data(self, o):
        return {
            "salesperson": f"{o.salesperson.last_name}, {o.salesperson.first_name}",
            "automobile": o.automobile.vin,
            "customer": f"{o.customer.last_name}, {o.customer.first_name}",
        }


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "phone_number", "address"]


@require_http_methods(["GET", "POST"])
def list_salesperson_view(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            salespersons,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        new_salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            new_salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_sale_view(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            sales,
            encoder=SaleEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            content["automobile"] = AutomobileVO.objects.get(vin=content["automobile"])
            content["customer"] = Customer.objects.get(phone_number=content["customer"])
            content["salesperson"] = Salesperson.objects.get(
                employee_id=content["salesperson"]
            )
        except KeyError:
            return JsonResponse({"message": "Invalid data"})
        new_sale = Sale.objects.create(**content)
        return JsonResponse(
            new_sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_customer_view(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            customers,
            encoder=CustomerListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        new_customer = Customer.objects.create(**content)
        return JsonResponse(
            new_customer,
            encoder=CustomerListEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def list_automobile_view(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            automobiles,
            encoder=AutomobileEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def detail_salesperson_view(request, id):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.delete(id=id)
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        updated_salesperson = Salesperson.objects.update(id=id, **content)
        return JsonResponse(
            updated_salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def detail_sale_view(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.delete(id=id)
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        updated_sale = Sale.objects.update(id=id, **content)
        return JsonResponse(
            updated_sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def detail_customer_view(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.delete(id=id)
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        updated_customer = Customer.objects.update(id=id, **content)
        return JsonResponse(
            updated_customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def detail_automobile_view(request, id):
    if request.method == "GET":
        automobile = AutomobileVO.objects.get(id=id)
        return JsonResponse(
            automobile,
            encoder=AutomobileEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = AutomobileVO.objects.delete(id=id)
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        updated_automobile = AutomobileVO.objects.update(id=id, **content)
        return JsonResponse(
            updated_automobile,
            encoder=AutomobileEncoder,
            safe=False,
        )
