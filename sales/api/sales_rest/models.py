from django.db import models
from django.urls import reverse


# Create your models here.
class Salesperson(models.Model):
    first_name = models.CharField(max_length=75)
    last_name = models.CharField(max_length=75)
    employee_id = models.CharField(max_length=11)

    class Meta:
        verbose_name = "Salesperson"
        verbose_name_plural = "Salespersons"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Salesperson_detail", kwargs={"pk": self.pk})


class Customer(models.Model):
    first_name = models.CharField(max_length=75)
    last_name = models.CharField(max_length=75)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=10)

    class Meta:
        verbose_name = "Customer"
        verbose_name_plural = "Customers"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Customer_detail", kwargs={"pk": self.pk})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField()

    class Meta:
        verbose_name = "AutomobileVO"
        verbose_name_plural = "AutomobileVOs"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("AutomobileVO_detail", kwargs={"pk": self.pk})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    price = models.BigIntegerField()

    class Meta:
        verbose_name = "Sale"
        verbose_name_plural = "Sales"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Sale_detail", kwargs={"pk": self.pk})
